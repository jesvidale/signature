export const files = {
  count: 6,
  results: [
    {
      id: 1,
      title: 'Guía para la actuación en el ámbito laboral en relacion al COVID-19',
      description: `
        <p class="parrafo_2">Las previsiones económicas de instituciones nacionales e internacionales, públicas y privadas muestran una recuperación gradual de la economía española gracias a una mayor estabilidad macroeconómica y a unas bases estructurales más sólidas.</p>
        <p class="parrafo">Una vez acometidas las reformas estructurales más urgentes para lograr la imprescindible estabilidad macroeconómica, la prioridad, ahora que la actividad económica ha retornado a tasas de crecimiento positivas, es acelerar la recuperación y la creación de empleo.</p>
        <p class="parrafo">Por ello, el Gobierno ha dirigido toda su política económica al objetivo de la estabilización del mercado de trabajo en primer término, y de la creación de empleo en el contexto de crecimiento económico.</p>
        <p class="parrafo">Recientemente se han comenzado a observar datos que certifican que el mercado laboral se ha estabilizado. La Encuesta de Población Activa del cuarto trimestre de 2013 reflejó creación neta de empleo en términos desestacionalizados, algo que no se observaba desde el primer trimestre de 2008. Las previsiones estiman que se producirá una creación de empleo neta en 2014, a pesar de que el crecimiento será todavía moderado al continuar el proceso de corrección de desequilibrios acumulados en el pasado.</p>
      `,
      releasedOn: '4/10/2021',
      image: '/lawyer.jpg',
      type: 'A'
    },
    {
      id: 2,
      title: 'Ley Orgánica 3/2018 de Protección de Datos Personales y garantía de los derechos digitales',
      description: `
      <p class="parrafo_2">Las previsiones económicas de instituciones nacionales e internacionales, públicas y privadas muestran una recuperación gradual de la economía española gracias a una mayor estabilidad macroeconómica y a unas bases estructurales más sólidas.</p>
      <p class="parrafo">Recientemente se han comenzado a observar datos que certifican que el mercado laboral se ha estabilizado. La Encuesta de Población Activa del cuarto trimestre de 2013 reflejó creación neta de empleo en términos desestacionalizados, algo que no se observaba desde el primer trimestre de 2008. Las previsiones estiman que se producirá una creación de empleo neta en 2014, a pesar de que el crecimiento será todavía moderado al continuar el proceso de corrección de desequilibrios acumulados en el pasado.</p>
    `,
      image: '',
      releasedOn: '4/10/2021',
      type: 'C'
    },
    {
      id: 3,
      title: 'Real Decreto-ley 28/2020, de 22 de septiembre, de trabajo a distancia.',
      description: `
      <p class="parrafo_2">Las previsiones económicas de instituciones nacionales e internacionales, públicas y privadas muestran una recuperación gradual de la economía española gracias a una mayor estabilidad macroeconómica y a unas bases estructurales más sólidas.</p>
      <p class="parrafo">Una vez acometidas las reformas estructurales más urgentes para lograr la imprescindible estabilidad macroeconómica, la prioridad, ahora que la actividad económica ha retornado a tasas de crecimiento positivas, es acelerar la recuperación y la creación de empleo.</p>
      <p class="parrafo">Por ello, el Gobierno ha dirigido toda su política económica al objetivo de la estabilización del mercado de trabajo en primer término, y de la creación de empleo en el contexto de crecimiento económico.</p>
      <p class="parrafo">Recientemente se han comenzado a observar datos que certifican que el mercado laboral se ha estabilizado. La Encuesta de Población Activa del cuarto trimestre de 2013 reflejó creación neta de empleo en términos desestacionalizados, algo que no se observaba desde el primer trimestre de 2008. Las previsiones estiman que se producirá una creación de empleo neta en 2014, a pesar de que el crecimiento será todavía moderado al continuar el proceso de corrección de desequilibrios acumulados en el pasado.</p>
    `,
      image: '',
      releasedOn: '2/10/2021',
      type: 'C'
    },
    {
      id: 4,
      title: 'Ley 39/2010, de 22 de diciembre, de Presupuestos Generales del Estado para el año 2011',
      description:  `
      <p class="parrafo_2">Las previsiones económicas de instituciones nacionales e internacionales, públicas y privadas muestran una recuperación gradual de la economía española gracias a una mayor estabilidad macroeconómica y a unas bases estructurales más sólidas.</p>
      <p class="parrafo">Una vez acometidas las reformas estructurales más urgentes para lograr la imprescindible estabilidad macroeconómica, la prioridad, ahora que la actividad económica ha retornado a tasas de crecimiento positivas, es acelerar la recuperación y la creación de empleo.</p>
      <p class="parrafo">Por ello, el Gobierno ha dirigido toda su política económica al objetivo de la estabilización del mercado de trabajo en primer término, y de la creación de empleo en el contexto de crecimiento económico.</p>
      <p class="parrafo">Recientemente se han comenzado a observar datos que certifican que el mercado laboral se ha estabilizado. La Encuesta de Población Activa del cuarto trimestre de 2013 reflejó creación neta de empleo en términos desestacionalizados, algo que no se observaba desde el primer trimestre de 2008. Las previsiones estiman que se producirá una creación de empleo neta en 2014, a pesar de que el crecimiento será todavía moderado al continuar el proceso de corrección de desequilibrios acumulados en el pasado.</p>
    `,
      image: '',
      releasedOn: '9/10/2021',
      type: 'C'
    },
    {
      id: 5,
      title: 'Real Decreto 1529/2012, de 8 de noviembre, por el que se desarrolla el contrato para la formación y el aprendizaje y se establecen las bases de la formación profesional dual',
      description:  `
      <p class="parrafo_2">Las previsiones económicas de instituciones nacionales e internacionales, públicas y privadas muestran una recuperación gradual de la economía española gracias a una mayor estabilidad macroeconómica y a unas bases estructurales más sólidas.</p>
      <p class="parrafo">Una vez acometidas las reformas estructurales más urgentes para lograr la imprescindible estabilidad macroeconómica, la prioridad, ahora que la actividad económica ha retornado a tasas de crecimiento positivas, es acelerar la recuperación y la creación de empleo.</p>
      <p class="parrafo">Por ello, el Gobierno ha dirigido toda su política económica al objetivo de la estabilización del mercado de trabajo en primer término, y de la creación de empleo en el contexto de crecimiento económico.</p>
      <p class="parrafo">Recientemente se han comenzado a observar datos que certifican que el mercado laboral se ha estabilizado. La Encuesta de Población Activa del cuarto trimestre de 2013 reflejó creación neta de empleo en términos desestacionalizados, algo que no se observaba desde el primer trimestre de 2008. Las previsiones estiman que se producirá una creación de empleo neta en 2014, a pesar de que el crecimiento será todavía moderado al continuar el proceso de corrección de desequilibrios acumulados en el pasado.</p>
    `,
      image: '',
      releasedOn: '',
      type: 'S'
    },
    {
      id: 6,
      title: 'Real Decreto-ley 3/2014, de 28 de febrero, de medidas urgentes para el fomento del empleo y la contratación indefinida.',
      description:  `
      <p class="parrafo_2">Las previsiones económicas de instituciones nacionales e internacionales, públicas y privadas muestran una recuperación gradual de la economía española gracias a una mayor estabilidad macroeconómica y a unas bases estructurales más sólidas.</p>
      <p class="parrafo">Una vez acometidas las reformas estructurales más urgentes para lograr la imprescindible estabilidad macroeconómica, la prioridad, ahora que la actividad económica ha retornado a tasas de crecimiento positivas, es acelerar la recuperación y la creación de empleo.</p>
      <p class="parrafo">Por ello, el Gobierno ha dirigido toda su política económica al objetivo de la estabilización del mercado de trabajo en primer término, y de la creación de empleo en el contexto de crecimiento económico.</p>
      <p class="parrafo">Recientemente se han comenzado a observar datos que certifican que el mercado laboral se ha estabilizado. La Encuesta de Población Activa del cuarto trimestre de 2013 reflejó creación neta de empleo en términos desestacionalizados, algo que no se observaba desde el primer trimestre de 2008. Las previsiones estiman que se producirá una creación de empleo neta en 2014, a pesar de que el crecimiento será todavía moderado al continuar el proceso de corrección de desequilibrios acumulados en el pasado.</p>
    `,
      image: '',
      releasedOn: '',
      type: 'S'
    },
    {
      id: 7,
      title: 'Real Decreto Legislativo 2/2015, de 23 de octubre, por el que se aprueba el texto refundido de la Ley del Estatuto de los Trabajadores.',
      description:  `
      <p class="parrafo_2">El artículo Uno.d) de la Ley 20/2014, de 29 de octubre, por la que se delega en el Gobierno la potestad de dictar diversos textos refundidos, en virtud de lo establecido en el artículo 82 y siguientes de la Constitución Española, autorizó al Gobierno para aprobar un texto refundido en el que se integrasen, debidamente regularizadas, aclaradas y armonizadas, el texto refundido de la Ley del Estatuto de los Trabajadores, aprobado mediante Real Decreto Legislativo 1/1995, de 24 de marzo, y todas las disposiciones legales relacionadas que se enumeran en ese apartado, así como las normas con rango de ley que las hubieren modificado. El plazo para la realización de dicho texto era de doce meses a partir de la entrada en vigor de la citada Ley 20/2014, que tuvo lugar el 31 de octubre de 2014.</p>
      <p class="parrafo">Este real decreto legislativo ha sido sometido a consulta de las organizaciones sindicales y empresariales más representativas. Además, ha sido informado por el Consejo Económico y Social.</p>
      <p class="parrafo">En su virtud, a propuesta de la Ministra de Empleo y Seguridad Social, de acuerdo con el Consejo de Estado y previa deliberación del Consejo de Ministros en su reunión del día 23 de octubre de 2015,</p>
      <p class="centro_redonda">DISPONGO:</p>
    `,
      image: '',
      releasedOn: '',
      type: 'S'
    },
    {
      id: 9,
      title: 'Real Decreto-ley 3/2014, de 28 de febrero, de medidas urgentes para el fomento del empleo y la contratación indefinida.',
      description:  `
      <p class="parrafo_2">Las previsiones económicas de instituciones nacionales e internacionales, públicas y privadas muestran una recuperación gradual de la economía española gracias a una mayor estabilidad macroeconómica y a unas bases estructurales más sólidas.</p>
      <p class="parrafo">Una vez acometidas las reformas estructurales más urgentes para lograr la imprescindible estabilidad macroeconómica, la prioridad, ahora que la actividad económica ha retornado a tasas de crecimiento positivas, es acelerar la recuperación y la creación de empleo.</p>
      <p class="parrafo">Por ello, el Gobierno ha dirigido toda su política económica al objetivo de la estabilización del mercado de trabajo en primer término, y de la creación de empleo en el contexto de crecimiento económico.</p>
      <p class="parrafo">Recientemente se han comenzado a observar datos que certifican que el mercado laboral se ha estabilizado. La Encuesta de Población Activa del cuarto trimestre de 2013 reflejó creación neta de empleo en términos desestacionalizados, algo que no se observaba desde el primer trimestre de 2008. Las previsiones estiman que se producirá una creación de empleo neta en 2014, a pesar de que el crecimiento será todavía moderado al continuar el proceso de corrección de desequilibrios acumulados en el pasado.</p>
    `,
      image: '',
      releasedOn: '',
      type: 'S'
    },
    {
      id: 10,
      title: 'Real Decreto-ley 3/2014, de 28 de febrero, de medidas urgentes para el fomento del empleo y la contratación indefinida.',
      description:  `
      <p class="parrafo_2">Las previsiones económicas de instituciones nacionales e internacionales, públicas y privadas muestran una recuperación gradual de la economía española gracias a una mayor estabilidad macroeconómica y a unas bases estructurales más sólidas.</p>
      <p class="parrafo">Una vez acometidas las reformas estructurales más urgentes para lograr la imprescindible estabilidad macroeconómica, la prioridad, ahora que la actividad económica ha retornado a tasas de crecimiento positivas, es acelerar la recuperación y la creación de empleo.</p>
      <p class="parrafo">Por ello, el Gobierno ha dirigido toda su política económica al objetivo de la estabilización del mercado de trabajo en primer término, y de la creación de empleo en el contexto de crecimiento económico.</p>
      <p class="parrafo">Recientemente se han comenzado a observar datos que certifican que el mercado laboral se ha estabilizado. La Encuesta de Población Activa del cuarto trimestre de 2013 reflejó creación neta de empleo en términos desestacionalizados, algo que no se observaba desde el primer trimestre de 2008. Las previsiones estiman que se producirá una creación de empleo neta en 2014, a pesar de que el crecimiento será todavía moderado al continuar el proceso de corrección de desequilibrios acumulados en el pasado.</p>
    `,
      image: '',
      releasedOn: '',
      type: 'S'
    }
  ]
}
