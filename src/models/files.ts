export class Files {
  files: File[]
  constructor () {
    this.files = []
  }

  get allFiles (): File[] {
    return this.files
  }

  get sortByNameAsc(): File[] {
    return this.sortFiles({order: 'asc'})
  }

  get sortByNameDesc(): File[] {
    return this.sortFiles({order: 'desc'})
  }

  newFile(file:File):File {
    let f = new File(file)
    this.files.push(f)
    return f
  }

  sortFiles({order}:{order:string}):File[] {
    const sorted = this.files.sort((a,b) => (a.title > b.title) ? 1 : ((b.title > a.title) ? -1 : 0))
    return order === 'asc' ? sorted : sorted.reverse()
  }
}

export class File implements InterfaceFile {
  id: number
  title: string
  description: string
  image: string
  releasedOn: string
  type: string
  constructor(file:{id:number, title: string, description: string, image: string, releasedOn: string, type: string}) {
    this.id = file.id
    this.title = file.title ? file.title : ''
    this.description = file.description
    this.image = file.image
    this.releasedOn = file.releasedOn
    this.type = file.type
  }
}

export interface InterfaceFile {
  id: number
  title: string
  description: string
  image: string
  releasedOn: string
  type: string
}
