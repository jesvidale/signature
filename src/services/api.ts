export const api = {
  requestFiles: () => {
    const { files } = require('../mocks/files')
    return files
  },
  requestDetail: (id:number) => {
    const { files } = require('../mocks/files')
    return files.results.find((el:any) => el.id === id)
  },
  deleteFile: (id:number):void => {
    alert(`POST: Delete element with identifier ${id}`)
  }
}
