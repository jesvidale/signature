import { File, Files } from '../models/files'
import { api } from './api'

export const getFiles = (): File[] => {
  const resp = api.requestFiles()
  let files = new Files()
  const filesList = resp?.results.map((el:File) => files.newFile(el))
  return filesList
}

export const getFileDetail = (id:number):File => {
  const resp = api.requestDetail(id)
  return new File(resp)
}

export const deleteFile = (id:number): string => {
  const resp = api.deleteFile(id)
  return `remove file ${resp}`
}

