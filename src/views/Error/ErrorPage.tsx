import { Link } from 'react-router-dom'
import PageWrapper from '../../containers/PageWrapper/PageWrapper'
import './ErrorPage.scss'

const ErrorPage = () => {
  return (
    <PageWrapper>
      <div className="section-wrapper">
        <div className="wrapper">
          <h2 className="page-title sr-only">Error 404</h2>
          <div className="notfound-wrapper">
            <img
              className="notfound-image"
              alt="OOPS"
              src={'/oops.png'}
            />
            <span className="notfound-title">404 - PÁGINA NO ENCONTRADA</span>
            <div className="notfound-description">
              <span>Es posible que la página que está buscando se haya eliminado si se cambió el nombre o no está disponible temporalmente</span>
            </div>
            <Link className="button button-primary notfound-button" to="/documentos">IR A LISTADO DE DOCUMENTOS</Link>
          </div>
        </div>
      </div>
    </PageWrapper>
  )
}

export { ErrorPage }
