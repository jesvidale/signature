import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom"

import {
  DOCUMENTS
} from '../constants/paths'

// View imports
import Documents from './Documents'
import Detail from './Detail'
import ErrorPage from './Error'

const Routes = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/">
          <Redirect to={DOCUMENTS}/>
        </Route>
          <Route exact path={DOCUMENTS} component={Documents} />
          <Route exact path={`${DOCUMENTS}/:fileId`} component={Detail} />
          <Route component={ErrorPage} />
      </Switch>
    </BrowserRouter>
  )
}

export { Routes }
