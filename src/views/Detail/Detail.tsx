import PageWrapper from '../../containers/PageWrapper/PageWrapper'
import Breadcrumb from '../../components/common/Breadcrumb/CommonBreadcrumb'
import './Detail.scss'
import parse from 'html-react-parser'
import { useState, useEffect } from 'react';
import { getFileDetail } from '../../services/files'
import { useHistory, useParams } from 'react-router'
import { Link } from 'react-router-dom';
import { InterfaceFile } from '../../models/files'
import { IoChevronBackOutline } from "react-icons/io5";

const Detail = () => {
  const { fileId }:{ fileId:string } = useParams()
  const history = useHistory()

  const breadcrumbs = [
    {
      title: 'Documentos',
      to: '/documentos'
    },
    {
      title: 'Detalle'
    }
  ]
  const [detail, setDetail] = useState<InterfaceFile | undefined>()

  useEffect(() => {
    try {
      const fileDetail = getFileDetail(parseInt(fileId))
      setDetail(fileDetail) 
    } catch (error) {
      console.error(`Error: No se encontraron entradas para el registro con id: ${fileId}`)
      history.push('/error')
    }
  }, [fileId, history])
  
  return (
    <PageWrapper>
      <div className="page-wrapper">
        <div className="section-wrapper">
          <div className="wrapper">
            <Breadcrumb breadcrumbs={breadcrumbs} />
            <h2 className="page-title">{detail?.title}</h2>
            <div className="document-description">{parse(`${detail?.description}`)}</div>
            { detail?.type === 'A' ?
            <div className="document-image-wrapper">
              <img src={detail?.image} alt="detalle"/>
            </div>
            :<></>}
            { detail?.type === 'C' || detail?.type === 'A' ?
            <span className="document-released-on">Fecha de publicacion: {detail?.releasedOn}</span>
            :<></>}
            <div className="document-handler-wrapper">
              <Link className="document-return-link" to='/documentos'>
                <IoChevronBackOutline />
                Volver al listado de documentos
              </Link>
            </div>
          </div>
        </div>
      </div>
    </PageWrapper>
  )
}

export { Detail }
