// components
import PageWrapper from '../../containers/PageWrapper/PageWrapper'
import Breadcrumb from '../../components/common/Breadcrumb/CommonBreadcrumb'
import DocumentItem from '../../components/document/Item'
import DocumentFilter from '../../components/document/Filter'
import DocumentRemove from '../../components/document/Remove'
import DocumentAdd from '../../components/document/Add'
import './Documents.scss';
import ReactPaginate from 'react-paginate';
import {  useState } from 'react';
import { getFiles, deleteFile } from '../../services/files'

interface Documet {
  id: number
  title: string
}

interface PageSegment {
  init: number
  end: number
}

const Documents = () => {
  const breadcrumbs = [
    {
      title: 'Documentos'
    }
  ]
  const files = getFiles()
  const [list, setList] = useState(files)
  const [page, setPage] = useState(0)
  const [toRemove, setToRemove] = useState({id: 0, title:''})

  const filterFiles = (filterType:string) => {
    const res = files.filter(file => file.type === filterType)
    res.length ? setList(res) : setList(files)
  }

  const sliceFiles = ():PageSegment => {
    return {
      init: page === 0 ? 0 : (page)*5,
      end: (page + 1) * 5
    }
  }
  
  const removeTentative = ({id, title}: Documet):void => {
    setToRemove({id, title})
  }

  const removeFile = () => {
    deleteFile(toRemove.id)
    removeTentative({id: 0, title: ''})
  }
  
  return (
    <PageWrapper>
      <div className="page-wrapper">
        <div className="section-wrapper">
          <div className="wrapper">
            <Breadcrumb breadcrumbs={breadcrumbs} />
            <h2 className="page-title">Listado de documentos</h2>
            { !files.length ? 
            <div className="files-section">
              <div className="files-empty">
                <img className="files-empty-image" src={'/empty_list.webp'} title="Pedido sin artículos" alt="Pedido sin artículos"/>
                <div className="files-empty-text">No se ha encontrado ningún fichero</div>
              </div>
            </div>
            :
            <>
            <div className="files-handler">
              <DocumentAdd />
              <DocumentFilter filterFiles={filterFiles}/>
            </div>
            { toRemove.id ?
              <DocumentRemove
                title={toRemove.title}
                id={toRemove.id}
                reject={removeTentative}
                accept={removeFile}
              />:<></>}
            <div className="files">
              <ul className="files-summary">
                { list.slice(sliceFiles().init, sliceFiles().end).map(({id, title, type}, index) =>
                  <li
                    key={`files-${index}`}
                    className="files-summary-item"
                  >
                  <DocumentItem
                    id={id}
                    title={title}
                    type={type}
                    removeFile={removeTentative}
                  />
                </li>
                )}
              </ul>
              <ReactPaginate
                previousLabel={'<'}
                nextLabel={'>'}
                breakLabel={'...'}
                breakClassName={'break-me'}
                pageCount={Math.ceil(list.length / 5)}
                marginPagesDisplayed={1}
                pageRangeDisplayed={5}
                containerClassName={'pagination'}
                activeClassName={'active'}
                initialPage={0}
                onPageChange={data => setPage(data.selected)}
                disabledClassName={'disabled'}
              />
            </div>
            </>
          }
          </div>
        </div>
      </div>
    </PageWrapper>
  )
}

export { Documents }
