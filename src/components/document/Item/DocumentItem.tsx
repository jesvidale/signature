import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { IoEyeOutline, IoTrashOutline } from "react-icons/io5";
import './DocumentItem.scss'

const DocumentItem = ({id = 0, title = '', type = 'S', removeFile}:{id:number, title:string, type:string, removeFile: Function}) => {
  return (
  <div className="files-item">
    <div className="files-item-header">
      <DocType type={type} />
      <Link className="files-item-title" to={`/documentos/${id}`}>{ title }</Link>
    </div>
    <div className="files-item-handler">
      <Link
        className="button button-primary button-detail"
        to={`/documentos/${id}`}
        title="Ver detalle"
      >
        <IoEyeOutline title="Ver detalle" className="button-detail-icon" />
      </Link>
      <button
        className="button button-error button-trash"
        onClick={() => removeFile({id, title})}
        title="Eliminar"
      >
        <span className="sr-only">Eliminar</span>
        <IoTrashOutline title="Eliminar" className="button-trash-icon" />
      </button>
    </div>
  </div>
  )
}

const DocType = ({type}:{type:string}):JSX.Element => {
  switch (type) {
    case 'S':
      return (<span className="files-item-type" title="Simple">S</span>)
    case 'C':
      return (<span className="files-item-type" title="Customizado">C</span>)
    case 'A':
      return (<span className="files-item-type" title="Avanzado">A</span>)
    default:
      return <></>
  }
}

DocumentItem.propTypes = {
  id: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired
}

export default DocumentItem
