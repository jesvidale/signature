import { useState } from 'react'
import { IoChevronUpOutline } from "react-icons/io5";
import './DocumentFilter.scss'

const DocumentFilter = ({filterFiles}:{filterFiles: Function}) => {
  const [roll, setRoll] = useState(true)
  const handleClick = (type:string) => {
    setRoll(true)
    filterFiles(type)
  }
  return (
    <div className={`files-sort-order ${roll?'rolled': 'unrolled'}`}>
      <button className="button button-primary files-sort-roll" onClick={() => setRoll(!roll)}>
        <span className="files-sort-title">Filtrar tipo de documento</span>
        <IoChevronUpOutline className="files-sort-icon"/>
      </button>
      <ul className="files-order-list">
        <li className="files-order-item" tabIndex={0} onClick={() => handleClick('S')}>Simple</li>
        <li className="files-order-item" tabIndex={0} onClick={() => handleClick('C')}>Customizado</li>
        <li className="files-order-item" tabIndex={0} onClick={() => handleClick('A')}>Avanzado</li>
        <li className="files-order-item" tabIndex={0} onClick={() => handleClick('T')}>Ver todos</li>
      </ul>
    </div>
  )
}

export default DocumentFilter
