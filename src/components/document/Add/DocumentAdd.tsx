import { useState } from 'react'
import { IoAddOutline, IoCloseOutline } from "react-icons/io5";
import './DocumentAdd.scss'

const DocumentAdd = () => {
  const [visible, setVisible] = useState(false)
  return (
    <>
    <button className="button button-secondary add-button" onClick={() => setVisible(!visible)}>
      <IoAddOutline className="add-button-icon" />
      <span className="add-button-title">Añadir nuevo documento</span>
    </button>
    <div className={`modal-wrapper add-modal ${visible ? '': 'modal-wrapper-disabled'}`}>
      <div className="modal">
        <button className="modal-close" onClick={() => setVisible(false)}>
          <IoCloseOutline className="add-button-icon" />
        </button>
        <div className="add-modal-content">
          <span className="add-modal-title">Añadir nuevo documento</span>
          <span>(Crear formulario de añadir documento aquí)</span>
        </div>
      </div>
    </div>
  </>
  )
}

export default DocumentAdd
