import PropTypes from 'prop-types';
import { IoCloseOutline } from "react-icons/io5";
import './DocumentRemove.scss'

const DocumentRemove = ({id = 0, title = '', accept, reject}: {id: number, title: string, accept:Function, reject:Function}) => {
  return (
    <div className="modal-wrapper remove-modal">
      <div className="modal">
        <button className="modal-close" onClick={() => reject({id:0, title: ''})}>
          <IoCloseOutline className="modal-close-icon"/>
        </button>
        <div className="remove-modal-content">
          <span className="remove-modal-title">¿Desea eliminar el elemento?</span>
          <span className="remove-modal-title-desc">{ title }</span>
          <div className="remove-modal-handler">
            <button className="button button-success" onClick={() => accept()}>Aceptar</button>
            <button className="button button-error" onClick={() => reject({id:0, title: ''})}>Cancelar</button>
          </div>
        </div>
      </div>
    </div>
  )
}

DocumentRemove.propTypes = {
  title: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired
}
export default DocumentRemove
