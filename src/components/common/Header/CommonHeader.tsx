import './CommonHeader.scss';

const Header = () => {
  return (
    <div className="header">
      <div className="wrapper">
        <div className="header-container">
          <h1 className="header-title">Visor de documentos contractuales</h1>
        </div>
      </div>
    </div>
  )
}

export default Header;

