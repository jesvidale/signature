import './CommonFooter.scss';
import  { IoLogoFacebook, IoLogoTwitter, IoLogoLinkedin, IoLogoYoutube } from "react-icons/io5";

const Footer = () => (
  <div className="footer">
    <div className="wrapper">
      <div className="footer-content">
        <div className="footer-copyright">
          <span className="footer-copyright-title">Aviso legal</span>
          <span className="footer-copyright-author">Copyright (c) 2021 Jesus Test All Rights Reserved</span>
          <span className="footer-copyright-author">This product is protected by copyright and distributed under licenses restricting copying, distribution, and decompilation.</span>
        </div>
        <div className="footer-media">
          <span className="footer-media-title">Siguenos</span>
          <ul className="footer-media-links">
            <li className="footer-media-link">
              <a
                target="_blank"
                rel="noreferrer"
                href="https://www.facebook.com/"
                title="facebook"
              >
                <IoLogoFacebook className="footer-media-icon"/>
              </a>
            </li>
            <li className="footer-media-link">
              <a
                target="_blank"
                rel="noreferrer"
                href="https://twitter.com/lang=es"
                title="twitter"
              >
                <IoLogoTwitter className="footer-media-icon"/>
              </a>
            </li>
            <li className="footer-media-link">
              <a
                target="_blank"
                rel="noreferrer"
                href="https://www.linkedin.com/"
                title="linkedin"
              >
                <IoLogoLinkedin className="footer-media-icon" />
              </a>
            </li>
            <li className="footer-media-link">
              <a
                target="_blank"
                rel="noreferrer"
                href="https://www.youtube.com/"
                title="youtube"
              >
                <IoLogoYoutube className="footer-media-icon" />
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
)

export default Footer
