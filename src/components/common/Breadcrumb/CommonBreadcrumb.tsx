import PropTypes from 'prop-types';
import { Link } from 'react-router-dom'
import './CommonBreadcrumb.scss';

interface InterfaceBreadcrumb {
  to?: string
  title: string
}

const Breadcrumb = ({breadcrumbs}:{breadcrumbs:InterfaceBreadcrumb[]}) => {
  const BreadCrumbLink = (breadcrumb:InterfaceBreadcrumb):JSX.Element => {
    const { to, title } = breadcrumb
    if (to !== undefined && to !== null) {
      return (
        <Link
          className="breadcrumb-item"
          to={to}
        >
          <span className="breadcrumb-separator">/</span>
          <span className="breadcrumb-title">{ title }</span>
        </Link>
      )
    } else {
      return ( 
        <div className="breadcrumb-item">
          <span className="breadcrumb-separator">/</span>
          <span className="breadcrumb-title">{ title }</span>
        </div>
      )
    }
  }
  return (
    <div className="breadcrumb">
      <ul className="breadcrumb-list">
        <li className="breadcrumb-item">
          <Link className="breadcrumb-item" to={'/'}>
            <i className="breadcrumb-icon" />
            <span className="breadcrumb-title">Inicio</span>
          </Link>
        </li>
        { breadcrumbs.map((breadcrumb, index) =>
          <li
            key={`bread-${index}`}
            className="breadcrumb-item"
          >
            { BreadCrumbLink(breadcrumb) }
          </li>
        )}
      </ul>
    </div>
  )
}

Breadcrumb.propTypes = {
  breadcrumbs: PropTypes.arrayOf(PropTypes.shape({
    to: PropTypes.string,
    title: PropTypes.string.isRequired
  }))
}

Breadcrumb.defaultProps = {
  breadcrumbs: []
}

export default Breadcrumb
