import styled, { css, StyledProps } from 'styled-components';

export const Wrapper = styled.button`
  text-decoration: none;
  font-size: 14px;
  font-weight: bold;
  padding: 10px 16px;
  border-radius: 5px;
  border: 1px solid $primary;
  cursor: pointer;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  height: 40px;
`

const filled = css`
  background-color: $primary;
  color: $white;
  &:hover {
    background-color: darken($primary, 10%);;
  }
`

const outlined = css`
  background-color: white;
  color: $primary;
  &:hover {
    color: darken($primary, 10%);
  }
`
const error = css`
  background-color: $error;
  color: white;
  border: 1px solid $error;
  &:hover {
    background-color: darken($error, 10%);
  }
`

const success = css`
  background-color: $success;
  color: white;
  border: 1px solid $success;
  &:hover {
    background-color: darken($success, 10%);
  }
`

const look = (props: StyledProps<{ look: 'filled' | 'outlined' | 'error' | 'success' }>) => {
  switch (props.look) {
    case 'filled':
      return filled;
    case 'outlined':
      return outlined;
    case 'error':
      return error;
    case 'success':
      return success;
    default:
      return filled;
  }
};
