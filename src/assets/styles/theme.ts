import { DefaultTheme } from 'styled-components';

const theme: DefaultTheme = {
  colors: {
    white: '#fff',
    blue: '#3EB4FF',
    black: '#232323',
    grayFooter: '#2a2c2f',
    grayHeader: '#27313c',
    boxShadow: '#d1d1d1',
    greyBack: '#f7f7f7',
    greyLight: '#e4e4e4',
    grey: '#999',
    greyFont: '#888',
    greyTable: '#f8f8f8',
    primary: '#503c88',
    secondary: '#FBC50F',
    error: '#fd615e',
    success: '#4BCA81'
  },
  font: {
    corporate: 'Montserrat, sans-serif',
    corporateSemi: 'Montserrat semibold, sans-serif',
    secondary: 'Lato, sans-serif',
    size: '16px'
  },
  breakpoints: {
    mobile: '(max-width: 767px)',
    tablet: '(min-width: 768px)',
    laptop: '(min-width: 1024px)',
    desktop: '(min-width: 1366px)',
    fullhd: '(min-width: 1920px)'
  }
}

export { theme }
